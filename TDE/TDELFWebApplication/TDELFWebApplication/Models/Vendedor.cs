﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TDELFWebApplication.Models
{
    public class Vendedor
    {
        public int id { get; set; }
        public string nome { get; set; }
        public string empresa  { get; set; }

        public List<Venda> Vendas { get; set; }
    }
}
