﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TDELFWebApplication.Models
{
    public class Venda
    {
        public int id { get; set; }
        public int idItemVenda { get; set; }
        public int idCliente { get; set; }
        public int idVendedor { get; set; }
        public float valorTotal { get; set; }
    }
}
