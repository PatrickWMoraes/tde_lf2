import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'react-bootstrap';
import { Navbar, NavDropdown, Nav } from 'react-bootstrap';
import { BrowserRouter, Switch, Route, Link } from 'react-router-dom'
import { Home } from './components/Home';
import {Cliente} from './components/Cliente';
import {Produto} from './components/Produto';
import {Vendedor} from './components/Vendedor';
//utilizando o navbar para criação de componentes 
function App() {
  return (
    <Container className="Container">
      <BrowserRouter>
      
        <Navbar bg='light' expand='lg'>
          <Navbar.Brand as={Link} to="/">Merca do Braz</Navbar.Brand>
          <Navbar.Toggle aria-controls='basic-navbar-nav' />
          <Navbar.Collapse id='basic-navbar-nav'>
            <Nav className='mr-auto'>
              
              
                <NavDropdown.Item as={Link} to='/Clientes'>Clientes</NavDropdown.Item>       
                <NavDropdown.Item as={Link} to='/Vendedores'>Vendedores</NavDropdown.Item>
                <NavDropdown.Item as={Link} to='/Produtos'>Produtos</NavDropdown.Item>
              
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <Switch>
          <Route path="/" exact={true} component={Home} />
          <Route path="/Clientes" component={Cliente} />
          <Route path="/Produtos" component={Produto} />
          <Route path="/Vendedores" component={Vendedor} />
          
        </Switch>
      </BrowserRouter>
    </Container>
  );
}

export default App;
