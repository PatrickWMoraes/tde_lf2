import React, { Component } from 'react';
import { Button, Form, Modal } from 'react-bootstrap';
import { Table } from 'reactstrap';
import './App.css';

export class Produto extends Component {

  constructor(props) {
    super(props);

    this.state = {
      id: 0,
      nome: '',
      preco: 0.0,
      produtos: [],
      modalAberta: false
    };

    this.buscarProdutos = this.buscarProdutos.bind(this);
    this.buscarProdutosID = this.buscarProdutosID.bind(this);
   // this.inserirProduto = this.inserirProduto.bind(this);
    this.atualizarProduto = this.atualizarProduto.bind(this);
   // this.excluirProdutoID = this.excluirProdutoID.bind(this);
    this.renderTabela = this.renderTabela.bind(this);
    this.abrirModalInserir = this.abrirModalInserir.bind(this);
    this.fecharModal = this.fecharModal.bind(this);
    this.atualizaNome = this.atualizaNome.bind(this);
    this.atualizaPreco = this.atualizaPreco.bind(this);
  }

  componentDidMount() {
    this.buscarProdutos();
  }

  // GET - pega todos os produtos
  buscarProdutos() {
    fetch('https://localhost:44324/api/Produtoes/')
      .then(response => response.json())
      .then(data => this.setState({ produtos: data }));
  }
  
  //GET -pega um produto pelo id
  buscarProdutosID(id) {
    fetch('https://localhost:44324/api/Produtoes/' + id)
      .then(response => response.json())
      .then(data => this.setState(
        {
          id: data.id,
          nome: data.nome,
          preco: data.preco
        }));
  }

  inserirProduto = (produto) => {
    fetch('https://localhost:44324/api/Produtoes/', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(produto)
    }).then((resposta) => {

      if (resposta.ok) {
        this.buscarprodutos();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    }).catch(console.log);
  }

  //atualizar as informaçoes de um produto de acordo com o id
  atualizarProduto(produto) {
    fetch('https://localhost:44324/api/Produtoes/' + produto.id, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(produto)
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscarProdutos();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }
//exclui um produto de acordo com o id 
  excluirProdutoID = (id) => {
    fetch('https://localhost:44324/api/Produtoes/' + id, {
      method: 'DELETE',
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscarProdutos();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }

  atualizaNome(e) {
    this.setState({
      nome: e.target.value
    });
  }

  atualizaPreco(e) {
    this.setState({
      preco: e.target.value
    });
  }

  abrirModalInserir() {
    this.setState({
      modalAberta: true
    })
  }

  abrirModalAtualizar(id) {
    this.setState({
      id: id,
      modalAberta: true
    });

    this.buscarProdutosID(id);
  }

  fecharModal() {
    this.setState({
      id: 0,
      nome: "",
      preco: "",
      modalAberta: false
    })
  }

  submit = () => {
    const produto = {
      id: this.state.id,
      nome: this.state.nome,
      preco: this.state.preco
    };

    if (this.state.id === 0) {
      this.inserirproduto(produto);
    } else {
      this.atualizarProduto(produto);
    }
  }

  renderModal() {
    return (
      <Modal show={this.state.modalAberta} onHide={this.fecharModal}>
        <Modal.Header closeButton>
          <Modal.Title>Cadastra produto</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form id="modalForm" onSubmit={this.submit}>
            <Form.Group>
              <Form.Label>Nome</Form.Label>
              <Form.Control type='text' placeholder='Nome do produto' value={this.state.nome} onChange={this.atualizaNome} />
            </Form.Group>
            <Form.Group>
              <Form.Label>Preço</Form.Label>
              <Form.Control input type="number" step="any" placeholder='Preco' value={this.state.preco} onChange={this.atualizaPreco} />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.fecharModal}>
            Cancelar
      </Button>
          <Button variant="success" form="modalForm" type="submit">
            Cadastrar
      </Button>
        </Modal.Footer>
      </Modal>
    );
  }


  renderTabela() {
    return (
      <Table dark>
        <thead>
          <tr>
            <th>Nome</th>
            <th>Preço</th>
            <th> id</th>
            <th>Opções</th>
          </tr>
        </thead>
        <tbody>
          {this.state.produtos.map((produto) => (
            <tr key={produto.id}>
              <td>{produto.nome}</td>
              <td>{produto.preco}</td>
              <td>{produto.id}</td>
              <td>

                <div>
                  <Button variant="link" onClick={() => this.abrirModalAtualizar(produto.id)}>Atualizar</Button>
                  <Button variant="link" onClick={() => this.excluirProdutoID(produto.id)}>Excluir</Button>
                </div>
              </td>
            </tr>
          ))}
          

      
        

        </tbody>
      </Table>

    );


    
  }


  

  render() {
    return (
      <div>
         <div><p> Para fazer o cadastro de um Produto basta selecionao o botão "Cadastra Produto", para alterar uma informação basta ir em "Atualizar" e para excluir um produto da lista basta selecionar "Excluir" . </p> </div>
        
        <Button variant="danger" className="button-novo" onClick={this.abrirModalInserir}>Cadastra produto</Button>
        {this.renderTabela()}
        {this.renderModal()}
      </div>
    );
  }
}

export default Produto;