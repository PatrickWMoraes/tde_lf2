import React, { Component } from 'react';
import { Button, Form, Modal } from 'react-bootstrap';
import './App.css';
import { Table } from 'reactstrap';

export class Cliente extends Component {

  constructor(props) {
    super(props);
// criação do state para carregar ar informaçoes contidas dentro de cada atributo 
    this.state = {
      id: 0,
      nome: '',
      email: '',
      clientes: [],
      modalAberta: false
    };

    this.buscaClientes = this.buscaClientes.bind(this);
    this.buscaClienteID = this.buscaClienteID.bind(this);
   // this.inserirCliente = this.inserirCliente.bind(this);
    this.atualizarCliente = this.atualizarCliente.bind(this);
    //this.excluirClienteID = this.excluirClienteID.bind(this);
    this.renderTabela = this.renderTabela.bind(this);
    this.abrirModalInserir = this.abrirModalInserir.bind(this);
    this.fecharModal = this.fecharModal.bind(this);
    this.atualizaNome = this.atualizaNome.bind(this);
    this.atualizaEmail = this.atualizaEmail.bind(this);
  }
  //chama a função buscaClientes logo que é iniciado a aplicação
  componentDidMount() {
    this.buscaClientes();
  }

  // pegando a lista de clientes e armazenando em clientes
  buscaClientes() {
    fetch('https://localhost:44324/api/Clientes/')// endereço da api
      .then(response => response.json())
      .then(data => this.setState({ clientes: data }));
  }
  
  //GET (Funcionario com determinado id)
  buscaClienteID(id) {
    fetch('https://localhost:44324/api/Clientes/' + id)
      .then(response => response.json())
      .then(data => this.setState(
        {
          id: data.id,
          nome: data.nome,
          email: data.email
        }));
  }
// função para inserir cliente na lista atraves do POST
  inserirCliente = (cliente) => {
    fetch('https://localhost:44324/api/Clientes/', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(cliente)
    }).then((resposta) => {

      if (resposta.ok) {
        this.buscarclientes();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    }).catch(console.log);
  }
//função para atualizar as informaçoes de um cliente atravez do PUT
  atualizarCliente(cliente) {
    fetch('https://localhost:44324/api/Clientes/' + cliente.id, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(cliente)
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscaClientesID();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }
// função para deletar um cliente da lista atravez do DELETE
  excluirClienteID = (id) => {
    fetch('https://localhost:44324/api/Clientes/' + id, {
      method: 'DELETE',
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscaClientes();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }

  atualizaNome(e) {
    this.setState({
      nome: e.target.value
    });
  }

  atualizaEmail(e) {
    this.setState({
      email: e.target.value
    });
  }
//modal abre uma tela
  abrirModalInserir() {
    this.setState({
      modalAberta: true
    })
  }

  abrirModalAtualizar(id) {
    this.setState({
      id: id,
      modalAberta: true
    });

    this.buscaClienteID(id);
  }

  fecharModal() {
    this.setState({
      id: 0,
      nome: "",
      email: "",
      modalAberta: false
    })
  }

  submit = () => {
    const cliente = {
      id: this.state.id,
      nome: this.state.nome,
      email: this.state.email
    };

    if (this.state.id === 0) {
      this.inserirCliente(cliente);
    } else {
      this.atualizarCliente(cliente);
    }
  }
  //criaçaão da model para o cadastro
  renderModal() {
    return (
      <Modal show={this.state.modalAberta} onHide={this.fecharModal}>
        <Modal.Header closeButton>
          <Modal.Title>Cadastra Cliente</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form id="modalForm" onSubmit={this.submit}>
            <Form.Group>
              <Form.Label>Nome</Form.Label>
              <Form.Control type='text' placeholder='Nome do Cliente' value={this.state.nome} onChange={this.atualizaNome} />
            </Form.Group>
            <Form.Group>
              <Form.Label>Email</Form.Label>
              <Form.Control type='email' placeholder='Email' value={this.state.email} onChange={this.atualizaEmail} />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
        <Button variant="secondary" onClick={this.fecharModal}>
            Cancelar
      </Button>
          <Button variant="success" form="modalForm" type="submit">
            Cadastrar
      </Button>
        </Modal.Footer>
      </Modal>
    );
  }


  renderTabela() {
    return (
      <Table dark>
        <thead>
          <tr>
            <th> Nome</th>
            <th>Email</th>
            <th>id</th>
            <th>Opções</th>
          </tr>
        </thead>
        <tbody>
          {this.state.clientes.map((cliente) => (
            <tr key={cliente.id}>
              <td>{cliente.nome}</td>
              <td>{cliente.email}</td>
              <td>{cliente.id}</td>
              <td>
                <div>
                  <Button variant="link" onClick={() => this.abrirModalAtualizar(cliente.id)}>Atualizar</Button>
                  <Button variant="link" onClick={() => this.excluirClienteID(cliente.id)}>Excluir</Button>
                </div>
              </td>
            </tr>
          ))}

               
        </tbody>
      </Table>
      
    );
    
  }

  render() {
    return (
      <div>
        <div><p> Para fazer o cadastro de um cliente basta selecionao o botão "Cadastra Cliente", para alterar uma informação basta ir em "Atualizar" e para excluir um nome da lista basta selecionar "Excluir" . </p> </div>
        

        <Button variant="danger" className="button-novo" onClick={this.abrirModalInserir}>Cadastra Cliente</Button>
        {this.renderTabela()}
        {this.renderModal()}
      </div>
    );
  }
 
  
}

export default Cliente;