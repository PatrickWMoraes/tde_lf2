import React, { Component } from 'react';
import { Button, Form, Modal } from 'react-bootstrap';
import { Table } from 'reactstrap';
import './App.css';

export class Vendedor extends Component {

  constructor(props) {
    super(props);

    this.state = {
      id: 0,
      nome: '',
      empresa: '',
      vendedores: [],
      modalAberta: false
    };

    this.buscaVendedores = this.buscaVendedores.bind(this);
    this.buscaVendedorID = this.buscaVendedorID.bind(this);
    //this.inserirVendedor = this.inserirVendedor.bind(this);
    this.atualizarVendedor = this.atualizarVendedor.bind(this);
    //this.excluirVendedorID = this.excluirVendedorID.bind(this);
    this.renderTabela = this.renderTabela.bind(this);
    this.abrirModalInserir = this.abrirModalInserir.bind(this);
    this.fecharModal = this.fecharModal.bind(this);
    this.atualizaNome = this.atualizaNome.bind(this);
    this.atualizaEmpresa = this.atualizaEmpresa.bind(this);
  }

  componentDidMount() {
    this.buscaVendedores();
  }

  // GET (todos vendedores)
  buscaVendedores() {
    fetch('https://localhost:44324/api/Vendedors/')
      .then(response => response.json())
      .then(data => this.setState({ vendedores: data }));
  }
  
  //GET (Vendedor com determinado id)
  buscaVendedorID(id) {
    fetch('https://localhost:44324/api/Vendedors/' + id)
      .then(response => response.json())
      .then(data => this.setState(
        {
          id: data.id,
          nome: data.nome,
          empresa: data.empresa
        }));
  }

  inserirVendedor = (vendedor) => {
    fetch('https://localhost:44324/api/Vendedors/', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(vendedor)
    }).then((resposta) => {

      if (resposta.ok) {
        this.buscarvendedores();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    }).catch(console.log);
  }
//funcção para atualizar as informaçoes de um vendedor atravez do PUT
  atualizarVendedor(vendedor) {
    fetch('https://localhost:44324/api/Vendedors/' + vendedor.id, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(vendedor)
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscaVendedorID();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }
// função para deletar um vendedor da lista atravez do DELETE
  excluirVendedorID = (id) => {
    fetch('https://localhost:44324/api/Vendedors/' + id, {
      method: 'DELETE',
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscaVendedores();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }

  atualizaNome(e) {
    this.setState({
      nome: e.target.value
    });
  }

  atualizaEmpresa(e) {
    this.setState({
      empresa: e.target.value
    });
  }
//modal abre uma tela
  abrirModalInserir() {
    this.setState({
      modalAberta: true
    })
  }

  abrirModalAtualizar(id) {
    this.setState({
      id: id,
      modalAberta: true
    });

    this.buscaVendedorID(id);
  }

  fecharModal() {
    this.setState({
      id: 0,
      nome: "",
      empresa: "",
      modalAberta: false
    })
  }

  submit = () => {
    const vendedor = {
      id: this.state.id,
      nome: this.state.nome,
      empresa: this.state.empresa
    };

    if (this.state.id === 0) {
      this.inserirVendedor(vendedor);
    } else {
      this.atualizarVendedor(vendedor);
    }
  }

  renderModal() {
    return (
      <Modal show={this.state.modalAberta} onHide={this.fecharModal}>
        <Modal.Header closeButton>
          <Modal.Title>Cadastra Vendedor</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form id="modalForm" onSubmit={this.submit}>
            <Form.Group>
              <Form.Label>Nome</Form.Label>
              <Form.Control type='text' placeholder='Nome do Vendedor' value={this.state.nome} onChange={this.atualizaNome} />
            </Form.Group>
            <Form.Group>
              <Form.Label>empresa</Form.Label>
              <Form.Control type='text' placeholder='Empresa' value={this.state.empresa} onChange={this.atualizaEmpresa} />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
        <Button variant="secondary" onClick={this.fecharModal}>
            Cancelar
      </Button>
          <Button variant="success" form="modalForm" type="submit">
            Cadastrar
      </Button>
        </Modal.Footer>
      </Modal>
    );
  }


  renderTabela() {
    return (
      <Table dark>
        <thead>
          <tr>
            <th> Nome</th>
            <th>Empresa</th>
            <th>id</th>
            <th>Opções</th>
          </tr>
        </thead>
        <tbody>
          {this.state.vendedores.map((vendedor) => (
            <tr key={vendedor.id}>
              <td>{vendedor.nome}</td>
              <td>{vendedor.empresa}</td>
              <td>{vendedor.id}</td>
              <td>
                <div>
                  <Button variant="link" onClick={() => this.abrirModalAtualizar(vendedor.id)}>Atualizar</Button>
                  <Button variant="link" onClick={() => this.excluirVendedorID(vendedor.id)}>Excluir</Button>
                </div>

              </td>
            </tr>
            

            
          ))}
        

        </tbody>
      </Table>
    );
  }

  render() {
    return (
      <div>
         <div><p> Para fazer o cadastro de um vendedor basta selecionao o botão "Cadastra Vendedor", para alterar uma informação basta ir em "Atualizar" e para excluir um nome da lista basta selecionar "Excluir" . </p> </div>
        
        <Button variant="danger" className="button-novo" onClick={this.abrirModalInserir}>Cadastra Vendedor</Button>
        {this.renderTabela()}
        {this.renderModal()}
      </div>
    );
  }
}

export default Vendedor;